let tap = require('tap');

/*
 * TOP LEVEL TESTING
 */

tap.pass('this is fine');

/*
 * BASIC SUBTEST
 */

tap.test('this is trivial', (t) => {
	t.plan(2);
	t.equal(1, 1);
	t.equal(2, 2);
})

/*
 * SIMPLE TODO FORMAT
 */

tap.test('this is a todo');

/*
 * TIMEOUT OPTION
 */

tap.test('this times out', {timeout: 100}, (t) => {
	return setTimeout(() => {
		//execution longer than timeout fails ignore internal tests
		t.equal(1,1);
		t.equal(1,2);
		t.end();
	}, 1000)
})

/*
 * HANDLES ASYNCHRONOUS EXECUTION
 */

function delay (ms) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve('result')
		}, ms)
	})
}

tap.test('This uses a promise', {timeout: 1000}, (t) => {
	return delay(100).then((result) => {
		return t.test('promise resolved', (t) => {
			t.match(result, 'result');
			t.end();
		})
	}).catch(t.threw);
});

/*
 * ALLOW FOR CUSTOM ASSERTIONS
 */

//Can't use fat arrow function as the callback bc of "this" binding
tap.Test.prototype.addAssert('custom', 1, function (answer, msg, extra){
	if (answer == 42) {
		return this.pass('Life, the Universe, and Everything');
	}
	else {
		return this.fail('But what is the question?');
	}
});

tap.test('this uses a custom assertion', (t) => {
	t.custom(42);
	t.end();
});

/*
 * OPTIONAL BDD STYLE
 */

tap.mochaGlobals();
let assert = require('chai').assert;
describe('It can use mocha-style testing', () => {
	context('with before() and after()', () => {
		let one;
		before(()=>{one = 1})
		after(()=>{one = 2})
		it('should assess them properly', () => {
			assert.equal(one,1);
		});
	});
});