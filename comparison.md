#Comparison of Mocha and Tap

A side by side comparison of features in Mocha and Tap:


| Mocha | Tap |
|---|---|
| **Syntax** |  |
| Follows BDD style, with a `describe()` block to declare the component being tested, inside which are nested a series of `it()` statements describing what the component should do. Within the `it()`, assertions are made. | At the top-most level, a test is run from the `tap` instance, with an assertion made directly at that level (e.g., `tap.pass()`). `tap.test()` Can also be used to declare multiple levels of subtest, with a passed function representing the new test instance inside the subtest. | 
| **Assertions** |  |
| Mocha has no default assertions. It can use Node.js's assert library, or any other pluggable assertion library. Chai is most often used. | Tap has a set of defined assertions that can be called from t.test. These mostly mirror the Node.js default library, with a wide range of synonyms for each. |
|  | Tap's `addAssert()` function allows for creation of custom assertions. |
| **Asychronous Operations** |  |
| To test an asynchronous function pass a callback `done` to the  `it()` block, then call `done()` within the callback. You can also pass an error to `done()` | Calling `t.end()` inside of a callback will end the test only at that point. |
| **Parallel vs. Serial** |  |
| Mocha runs tests serially. | Tap runs serially, with an option to run parallel tests using `t.jobs = n` where n is the number of threads. | 
| **Promises** |  |
| To test a function that returns a promise, a library like chai-as-promised adds an additional set of assertions. | Each instance of `tap.test()` is itself a Promise, and subtests can return promises, which will allow multiple tests to be strung together. |
| **Timeouts** |  |
| By default, all tests time out after 2000 milliseconds. Timeouts can be set at any level of a test, in a `describe` or `it` statement, with `this.timeout` or set to `this.timeout(0)` to remove the timeout altogether.  | Timeouts are set on a per test basis, as part of an option object passed as the second parameter. |
| Also includes a function, `this.slow()`, to display a flag for slow tests in reporting.  |  |
| **Exceptions** |  |
| Uncaught exceptions are treated as fails and mapped to the correct test cases. Can be tested by the assertion library. | Uncaught exceptions are treated as failing tests, and included in report. exceptions in asynchronous operations may appear out of order in the report, but will include context details. Includes Assertions for `t.throw` and `t.doesNotThrow()`, as well as `t.threw()` for asynchronous operations. |
| **Setup and Teardown** |  |
| Includes hooks for `before()`, `beforeEach()`, `after()`, and `afterEach()` to execute code at different points in the test pipeline. | Includes `beforeEach()` and `afterEach()` hooks, as well as a `tearDown()` hook that runs after eveything is done. Becuase tap tests are written in straight node.js code, the equivalents of `before()` and `after()` can be put straight into code. |
| **Usage** |  |
| Must be run with the `mocha` command, from either terminal or from npm. Can globally install mocha, or use binary from node_modeules  | Can be run with the `tap` command (global install or binary) to generate spec-style reporting, or with `node` to generate straight TAP output. |
| **Other Features** |  |
| Mulitple syntaxes: BDD, TDD, Exports, QUint (similar to tap), and Require  | Can write tests in BDD format by calling `tap.mochaGlobals()` |
| Compatible with a number of text editor and IDE plugins. | Include optional code coverage reporting via command line. |
| Multiple reporting options (including TAP) | Multiple reporting options, TAP output can be piped into other reporters. |


##Similar Features 

Both allow inclusive/exclusive testing with `.only` or `.skip`.

Both support ToDo's by leaving out a callback.

Both have extensive CLI options.

Both have Nyan Cat reporting.